var renderN = {
  wymiarr: 0,
  koniecRenderu: 0,
  tabDefault: [],
  tabCheck: [],
  wygrana: 0,
  licznikk: 0,
  zdjecieID: 2,
  ilosc_zdjec: 2, //ilosc minus 1
  aktualne_zdjecie: "url('pic2.png')",
  mms1: 0,
  mms2: 0,
  mms3: 0,
  ss1: 0,
  ss2: 0,
  mmin1: 0,
  mmin2: 0,
  hh1: 0,
  hh2: 0,
  trybGry: 3,
  nick: "",
  czasGracza: 0,
  trybWyniki: 0,
  uzupelnienie0: 0,
  main: function () {
    document.writeln("<div id='pojemnik'></div>");
  },
  reszta: function () {
    var main1 = document.getElementById("pojemnik");
    //header
    var header1 = document.createElement("header");
    main1.appendChild(header1);
    for (i = 1; i < 4; i++) {
      var sekcja1 = document.createElement("div");
      sekcja1.id = "sekcja" + i;
      header1.appendChild(sekcja1);
    }
    //sekcja1
    var strzalkaLewo = document.createElement("div");
    strzalkaLewo.id = "strzalka0";
    strzalkaLewo.className = "strzalka";
    document.getElementById("sekcja1").appendChild(strzalkaLewo);

    var zdjecie = document.createElement("div");
    zdjecie.id = "zdjecie";
    document.getElementById("sekcja1").appendChild(zdjecie);
    var zdjecie0 = document.createElement("div");
    var zdjecie1 = document.createElement("div");
    var zdjecie2 = document.createElement("div");
    var zdjecie3 = document.createElement("div");
    var zdjecie4 = document.createElement("div");
    zdjecie0.id = "zdjecie0";
    zdjecie0.className = "zdjecie";
    zdjecie0.style.backgroundImage = "url('pic2.png')";
    document.getElementById("zdjecie").appendChild(zdjecie0);
    zdjecie1.id = "zdjecie1";
    zdjecie1.className = "zdjecie";
    zdjecie1.style.backgroundImage = "url('pic0.png')";
    document.getElementById("zdjecie").appendChild(zdjecie1);
    zdjecie2.id = "zdjecie2";
    zdjecie2.className = "zdjecie";
    zdjecie2.style.backgroundImage = "url('pic1.png')";
    document.getElementById("zdjecie").appendChild(zdjecie2);

    zdjecie3.id = "zdjecie3";
    zdjecie3.className = "zdjecie";
    zdjecie3.style.backgroundImage = "url('pic2.png')";
    document.getElementById("zdjecie").appendChild(zdjecie3);
    zdjecie4.id = "zdjecie4";
    zdjecie4.className = "zdjecie";
    zdjecie4.style.backgroundImage = "url('pic0.png')";
    document.getElementById("zdjecie").appendChild(zdjecie4);

    var strzalkaPrawo = document.createElement("div");
    strzalkaPrawo.id = "strzalka1";
    strzalkaPrawo.className = "strzalka";
    document.getElementById("sekcja1").appendChild(strzalkaPrawo);
    //sekcja2
    var pojemnikBT = document.createElement("div");
    pojemnikBT.id = "pojemnikBT";
    document.getElementById("sekcja2").appendChild(pojemnikBT);
    for (i = 0; i < 4; i++) {
      var button1 = document.createElement("button");
      button1.id = "button" + i;
      button1.className = "przycisk";
      button1.innerHTML = i + 3 + " x " + (i + 3);
      pojemnikBT.appendChild(button1);
    }
    //sekcja3
    var pojemnikLicznik = document.createElement("div");
    pojemnikLicznik.id = "pojemnikLicznik";
    // document.getElementById("sekcja3").appendChild(pojemnikLicznik) //bez draggable
    document.getElementById("pojemnik").appendChild(pojemnikLicznik);
    var dot = document.createElement("div");
    dot.classList.add("kropka", "znak");
    var ddot = document.createElement("div");
    ddot.classList.add("dkropek", "znak");
    var dddot = document.createElement("div");
    dddot.classList.add("dkropek", "znak");
    // dddot.innerHTML = ":"
    var ms = document.createElement("div");
    ms.id = "ms";
    ms.className = "licznikMS";
    var s = document.createElement("div");
    s.id = "s";
    s.className = "licznik";
    var min = document.createElement("div");
    min.id = "min";
    min.className = "licznik";
    var h = document.createElement("div");
    h.id = "h";
    h.className = "licznik";
    pojemnikLicznik.appendChild(h);
    pojemnikLicznik.appendChild(dddot);
    pojemnikLicznik.appendChild(min);
    pojemnikLicznik.appendChild(ddot);
    pojemnikLicznik.appendChild(s);
    pojemnikLicznik.appendChild(dot);
    pojemnikLicznik.appendChild(ms);
    var ms1 = document.createElement("div");
    ms1.classList.add("liczbaMS");
    ms1.classList.add("c0");
    ms1.id = "ms1";
    ms.appendChild(ms1);
    var ms2 = document.createElement("div");
    ms2.classList.add("liczbaMS");
    ms2.classList.add("c0");
    ms2.id = "ms2";
    ms.appendChild(ms2);
    var ms3 = document.createElement("div");
    ms3.classList.add("liczbaMS");
    ms3.classList.add("c0");
    ms3.id = "ms3";
    ms.appendChild(ms3);
    var s1 = document.createElement("div");
    s1.classList.add("liczba");
    s1.classList.add("c0");
    s1.id = "s1";
    s.appendChild(s1);
    var s2 = document.createElement("div");
    s2.classList.add("liczba");
    s2.classList.add("c0");
    s2.id = "s2";
    s.appendChild(s2);
    var min1 = document.createElement("div");
    min1.classList.add("liczba");
    min1.classList.add("c0");
    min1.id = "min1";
    min.appendChild(min1);
    var min2 = document.createElement("div");
    min2.classList.add("liczba");
    min2.classList.add("c0");
    min2.id = "min2";
    min.appendChild(min2);
    var h1 = document.createElement("div");
    h1.classList.add("liczba");
    h1.classList.add("c0");
    h1.id = "h1";
    h.appendChild(h1);
    var h2 = document.createElement("div");
    h2.classList.add("liczba");
    h2.classList.add("c0");
    h2.id = "h2";
    h.appendChild(h2);

    //footer
    var footer1 = document.createElement("footer");
    main1.appendChild(footer1);
    var pojemnikLos = document.createElement("div");
    pojemnikLos.id = "pojemnikLos";
    footer1.appendChild(pojemnikLos);
    var losowanko = document.createElement("div");
    losowanko.id = "los";
    pojemnikLos.appendChild(losowanko);

    var tablicaWynikow = document.createElement("div");
    tablicaWynikow.id = "otwarcieWyniki";
    tablicaWynikow.innerHTML = "otworz tabelę z najlepszymi wynikami";
    footer1.appendChild(tablicaWynikow);

    //wygrana gracza - overlay
    var overlay1 = document.createElement("div");
    overlay1.id = "overlayWygrana";
    document.getElementsByTagName("body")[0].appendChild(overlay1);
    var komunikat1 = document.createElement("div");
    komunikat1.id = "komunikatWygrana";
    overlay1.appendChild(komunikat1);
    headerWygrana = document.createElement("header");
    footerWygrana = document.createElement("footer");
    //header
    headerWygrana.id = "headerWygrana";
    headerWygrana.innerHTML = "Wygrana!";
    footerWygrana.id = "footerWygrana";
    komunikat1.appendChild(headerWygrana);
    //footer
    komunikat1.appendChild(footerWygrana);
    var gora1 = document.createElement("div");
    var czasWygranej1 = document.createElement("div");
    gora1.id = "gora1";
    gora1.innerHTML = "Twój czas:";
    czasWygranej1.id = "czasWygranej";
    footerWygrana.appendChild(gora1);
    footerWygrana.appendChild(czasWygranej1);
    //input z nickiem gracza
    var nickGracza1 = document.createElement("div");
    nickGracza1.id = "nickGracza";
    nickGracza1.innerHTML = "Twój nick:</br><input type='text' name='nick'>";
    footerWygrana.appendChild(nickGracza1);
    var warning = document.createElement("div");
    warning.id = "warning";
    footerWygrana.appendChild(warning);

    //przycisk ZAMKNIJ wygrana
    var wyjscie1 = document.createElement("div");
    wyjscie1.id = "wyjscieWygrana";
    wyjscie1.className = "wyjscie";
    wyjscie1.innerHTML = "OK";
    footerWygrana.appendChild(wyjscie1);

    //tablica wyników - overlay
    var overlay2 = document.createElement("div");
    overlay2.id = "overlayWyniki";
    document.getElementsByTagName("body")[0].appendChild(overlay2);
    var komunikat2 = document.createElement("div");
    komunikat2.id = "komunikatWyniki";
    overlay2.appendChild(komunikat2);
    headerWyniki = document.createElement("header");
    footerWyniki = document.createElement("footer");
    headerWyniki.id = "headerWyniki";
    footerWyniki.id = "footerWyniki";
    komunikat2.appendChild(headerWyniki);
    //headerWyniki
    var tytul = document.createElement("div");
    tytul.id = "tytulWyniki";
    tytul.innerHTML = "Najlepsze wyniki";
    headerWyniki.appendChild(tytul);
    var przyciski = document.createElement("div");
    przyciski.id = "przyciskiWyniki";
    headerWyniki.appendChild(przyciski);
    for (i = 0; i < 4; i++) {
      var button2 = document.createElement("button");
      button2.id = "buttonWynik" + i;
      button2.className = "btWyniki";
      button2.innerHTML = "tryb " + (i + 3) + "x" + (i + 3);
      przyciski.appendChild(button2);
    }
    //footerWyniki
    komunikat2.appendChild(footerWyniki);
    var tabela = document.createElement("div");
    tabela.id = "tabelaWyniki";
    // tabela.innerHTML = "[tabela z wynikami]"
    footerWyniki.appendChild(tabela);
    //przycisk ZAMKNIJ wyniki
    var wyjscie2 = document.createElement("div");
    wyjscie2.id = "wyjscieWyniki";
    wyjscie2.className = "wyjscie";
    wyjscie2.innerHTML = "zamknij";
    footerWyniki.appendChild(wyjscie2);

    //kolory
    // var kolor = document.createElement("div")
    // kolor.id = "kolorPojemnik"
    // kolor.innerHTML = "<input type='color' id='kolor' value='#f4ecf7' onchange='kolorek()'></input>"
    // document.body.appendChild(kolor)

    //motywy kolorystyczne
    var motywPojemnik = document.createElement("div");
    motywPojemnik.id = "motywPojemnik";
    document.body.appendChild(motywPojemnik);
    var motyw1 = document.createElement("div"); //poszczegolne motywy strony
    motyw1.id = "motyw1";
    motyw1.classList = "motyw";
    var motyw2 = document.createElement("div");
    motyw2.id = "motyw2";
    motyw2.classList = "motyw";
    var motyw3 = document.createElement("div");
    motyw3.id = "motyw3";
    motyw3.classList = "motyw";
    motywPojemnik.appendChild(motyw1);
    motywPojemnik.appendChild(motyw2);
    motywPojemnik.appendChild(motyw3);
  },
  los: function () {
    renderZdjecia = function (c) {
      for (i = 0; i < 4; i++) {
        //blokada klikania przycisków podczas losowania
        document.getElementById("button" + i).removeEventListener("click", renderZdjecia);
        document.getElementById("button" + i).style.background = "var(--przezroczystosc-tla)";
        document.getElementById("button" + i).style.cursor = "not-allowed";
        document.getElementById("button" + i).disabled = true;
      }
      this.style.background = "var(--wybrany-tryb)"; //zaznaczenie wybranego trybu na przycisku
      var wymiar = this.innerHTML[0];
      renderN.wymiarr = this.innerHTML[0];
      var losowanko = document.getElementById("los");
      losowanko.innerHTML = "";
      losowanko.style.display = "block";
      var klasa = 0;
      renderN.tabDefault = [];
      for (i = 0; i < wymiar; i++) {
        for (j = 0; j < wymiar; j++) {
          klasa++;
          renderN.tabDefault.push("" + klasa); //dodanie elementow do defaultowej tablicy
          var div1 = document.createElement("div");
          var losowanko = document.getElementById("los");
          div1.className = "loss";
          div1.classList.add(klasa);
          if (i == wymiar - 1 && j == wymiar - 1) {
            div1.classList.add("czarny");
          }
          div1.id = "zd" + i + j;
          div1.style.left = Math.floor(100 / wymiar) * j + "%"; //pozycja
          div1.style.top = Math.floor(100 / wymiar) * i + "%";
          div1.style.width = Math.floor(100 / wymiar) + "%"; //wymiar poszczegolnych divow (podzielonych)
          div1.style.height = Math.floor(100 / wymiar) + "%";
          div1.style.backgroundPosition = Math.floor(100 / wymiar) * j + "% " + Math.floor(100 / wymiar) * i + "%";
          losowanko.appendChild(div1);
          div1.style.backgroundImage = renderN.aktualne_zdjecie;
        }
      }

      renderN.licznikk = 0;
      var stop = setInterval(losRender, 10);
      function losRender() {
        var kierunek = Math.floor(Math.random() * 4); // 0góra 1prawo 2dol 3lewo
        renderN.licznikk++;
        if (renderN.licznikk == wymiar * wymiar * 10) {
          //zatrzymanie losowania (wymiar * wymiar) * 10
          clearInterval(stop);
          renderN.koniecRenderu = 1;
          for (i = 0; i < 4; i++) {
            //ponowna mozliwosc klikania przycisków po losowaniu
            document.getElementById("button" + i).addEventListener("click", renderZdjecia);
            document.getElementById("button" + i).style.cursor = "pointer";
            document.getElementById("button" + i).disabled = false;
          }
          {
            //nowy licznik
            var ms1 = document.getElementById("ms1");
            var ms2 = document.getElementById("ms2");
            var ms3 = document.getElementById("ms3");
            var s1 = document.getElementById("s1");
            var s2 = document.getElementById("s2");
            var min1 = document.getElementById("min1");
            var min2 = document.getElementById("min2");
            var h1 = document.getElementById("h1");
            var h2 = document.getElementById("h2");
            var czasDefault = new Date().getTime();

            var stopLicznika = setInterval(licznikCzasu, 1);
            function licznikCzasu() {
              if (renderN.licznikk == 0) {
                clearInterval(stopLicznika);
              }
              nowyCzas = new Date().getTime(); //czas updatowany za kazdym "kliknieciem" intervalu
              roznica = parseInt((czasDefault - nowyCzas) * -1);

              sekundy = Math.floor(roznica / 1000);
              minuty = Math.floor(roznica / 1000 / 60);
              godziny = Math.floor(roznica / 1000 / 60 / 60);

              ms1.classList = "liczbaMS c" + roznica.toString().slice(-3, -2); //updatowanie licznika
              if (roznica.toString().slice(-3, -2) == "") {
                ms1.classList = "liczbaMS c0";
                renderN.mms1 = 0;
              }
              renderN.mms1 = roznica.toString().slice(-3, -2); //updatowanie zmiennej do pozniejszego wpisania do cookies i czasu wygranej

              ms2.classList = "liczbaMS c" + roznica.toString().slice(-2, -1);
              if (roznica.toString().slice(-2, -1) == "") {
                ms2.classList = "liczbaMS c0";
                renderN.mms2 = 0;
              }
              renderN.mms2 = roznica.toString().slice(-2, -1);

              ms3.classList = "liczbaMS c" + roznica.toString().slice(-1);
              renderN.mms3 = roznica.toString().slice(-1);

              if (sekundy >= 60) {
                sekundy = sekundy - 60 * minuty;
                s1.classList = "liczba c" + sekundy.toString().slice(-2, -1);
                renderN.ss1 = sekundy.toString().slice(-2, -1);
                if (sekundy.toString().slice(-2, -1) == "") {
                  s1.classList = "liczba c0";
                  renderN.ss1 = 0;
                }

                s2.classList = "liczba c" + sekundy.toString().slice(-1);
                renderN.ss2 = sekundy.toString().slice(-1);
              } else if (sekundy < 60) {
                s1.classList = "liczba c" + sekundy.toString().slice(-2, -1);
                renderN.ss1 = sekundy.toString().slice(-2, -1);
                if (sekundy.toString().slice(-2, -1) == "") {
                  s1.classList = "liczba c0";
                  renderN.ss1 = 0;
                }

                s2.classList = "liczba c" + sekundy.toString().slice(-1);
                renderN.ss2 = sekundy.toString().slice(-1);
              }

              if (minuty >= 60) {
                minuty = minuty - 60 * godziny;
                min1.classList = "liczba c" + minuty.toString().slice(-2, -1);
                renderN.mmin1 = minuty.toString().slice(-2, -1);
                if (minuty.toString().slice(-2, -1) == "") {
                  min1.classList = "liczba c0";
                  renderN.mmin1 = 0;
                }
                min2.classList = "liczba c" + minuty.toString().slice(-1);
                renderN.mmin2 = minuty.toString().slice(-1);
              } else if (minuty < 60) {
                min1.classList = "liczba c" + minuty.toString().slice(-2, -1);
                renderN.mmin1 = minuty.toString().slice(-2, -1);
                if (minuty.toString().slice(-2, -1) == "") {
                  min1.classList = "liczba c0";
                  renderN.mmin1 = 0;
                }
                min2.classList = "liczba c" + minuty.toString().slice(-1);
                renderN.mmin2 = minuty.toString().slice(-1);
              }

              h1.classList = "liczba c" + godziny.toString().slice(-2, -1);
              if (godziny.toString().slice(-2, -1) == "") {
                h1.classList = "liczba c0";
                renderN.hh1 = 0;
              }
              h2.classList = "liczba c" + godziny.toString().slice(-1);

              if (renderN.wygrana == 1) {
                document.getElementById("czasWygranej").innerHTML =
                  "" +
                  renderN.hh1 +
                  renderN.hh2 +
                  ":" +
                  renderN.mmin1 +
                  renderN.mmin2 +
                  ":" +
                  renderN.ss1 +
                  renderN.ss2 +
                  "." +
                  renderN.mms1 +
                  renderN.mms2 +
                  renderN.mms3;
                renderN.wygrana = 0;
                renderN.czasGracza = roznica;
                clearInterval(stopLicznika);
              }
            }
          }
        }
        if (kierunek == 0) {
          //"nad" czarnym
          var czarny = document.getElementsByClassName("czarny")[0];
          var topCzarny = czarny.style.top;
          var leftCzarny = czarny.style.left;

          var Sibling = document.getElementById("zd" + (parseInt(czarny.id[2]) - 1) + czarny.id[3]);
          if (Sibling != undefined) {
            var topSibling = Sibling.style.top;
            var leftSibling = Sibling.style.left;
            czarnyOldID = czarny.id; //zamiana wspolrzednych
            SiblingOldID = Sibling.id;
            czarny.style.top = topSibling; //zamiana pozycji czarnego
            czarny.style.left = leftSibling;
            czarny.id = SiblingOldID;
            Sibling.style.top = topCzarny; //zamiana pozycji sąsiadującego
            Sibling.style.left = leftCzarny;
            Sibling.id = czarnyOldID;
          }
        } else if (kierunek == 1) {
          //po prawo od czarnego
          var czarny = document.getElementsByClassName("czarny")[0];
          var topCzarny = czarny.style.top;
          var leftCzarny = czarny.style.left;

          var Sibling = document.getElementById("zd" + czarny.id[2] + (parseInt(czarny.id[3]) + 1));
          if (Sibling != undefined) {
            var topSibling = Sibling.style.top;
            var leftSibling = Sibling.style.left;
            czarnyOldID = czarny.id; //zamiana wspolrzednych
            SiblingOldID = Sibling.id;
            czarny.style.top = topSibling; //zamiana pozycji czarnego
            czarny.style.left = leftSibling;
            czarny.id = SiblingOldID;
            Sibling.style.top = topCzarny; //zamiana pozycji sąsiadującego
            Sibling.style.left = leftCzarny;
            Sibling.id = czarnyOldID;
          }
        } else if (kierunek == 2) {
          //"pod" czarnym
          var czarny = document.getElementsByClassName("czarny")[0];
          var topCzarny = czarny.style.top;
          var leftCzarny = czarny.style.left;

          var Sibling = document.getElementById("zd" + (parseInt(czarny.id[2]) + 1) + czarny.id[3]);
          if (Sibling != undefined) {
            var topSibling = Sibling.style.top;
            var leftSibling = Sibling.style.left;
            czarnyOldID = czarny.id; //zamiana wspolrzednych
            SiblingOldID = Sibling.id;
            czarny.style.top = topSibling; //zamiana pozycji czarnego
            czarny.style.left = leftSibling;
            czarny.id = SiblingOldID;
            Sibling.style.top = topCzarny; //zamiana pozycji sąsiadującego
            Sibling.style.left = leftCzarny;
            Sibling.id = czarnyOldID;
          }
        } else if (kierunek == 3) {
          //po lewo od czarnego
          var czarny = document.getElementsByClassName("czarny")[0];
          var topCzarny = czarny.style.top;
          var leftCzarny = czarny.style.left;

          var Sibling = document.getElementById("zd" + czarny.id[2] + (parseInt(czarny.id[3]) - 1));
          if (Sibling != undefined) {
            var topSibling = Sibling.style.top;
            var leftSibling = Sibling.style.left;
            czarnyOldID = czarny.id; //zamiana wspolrzednych
            SiblingOldID = Sibling.id;
            czarny.style.top = topSibling; //zamiana pozycji czarnego
            czarny.style.left = leftSibling;
            czarny.id = SiblingOldID;
            Sibling.style.top = topCzarny; //zamiana pozycji sąsiadującego
            Sibling.style.left = leftCzarny;
            Sibling.id = czarnyOldID;
          }
        }
      }
    };
    zmiana = function (d) {
      //zmiana zdjecia wyswietlanego u gory
      var zdjecie = document.getElementById("zdjecie");
      if (this.id[8] == 1) {
        //prawa strzalka
        renderN.zdjecieID = parseInt(renderN.zdjecieID) + 1;
        if (renderN.zdjecieID == parseInt(renderN.ilosc_zdjec) + 1) {
          //po pierwszym kliknieciu
          renderN.zdjecieID = 0;
        }
        var h3h3 = 0;
        var stopp = setInterval(zm, 1);
        function zm() {
          h3h3++;
          if (h3h3 >= 150) {
            clearInterval(stopp);
          }
          zdjecie.scrollLeft = Math.ceil(zdjecie.scrollLeft + 1);
          if (zdjecie.scrollLeft == 600) {
            zdjecie.scrollLeft = 150;
          }
        }
        renderN.aktualne_zdjecie = "url('pic" + renderN.zdjecieID + ".png')";
      }

      if (this.id[8] == 0) {
        //lewa strzalka
        renderN.zdjecieID = parseInt(renderN.zdjecieID) - 1;
        if (renderN.zdjecieID == -1) {
          renderN.zdjecieID = parseInt(renderN.ilosc_zdjec);
        }
        var h3h3 = 0;
        var stopp = setInterval(zm, 1);
        function zm() {
          h3h3++;
          if (h3h3 >= 150) {
            clearInterval(stopp);
          }
          zdjecie.scrollLeft = Math.ceil(zdjecie.scrollLeft - 1);
          if (zdjecie.scrollLeft == 0) {
            zdjecie.scrollLeft = 450;
          }
        }
        renderN.aktualne_zdjecie = "url('pic" + renderN.zdjecieID + ".png')";
      }
    };
    wyjscieWygrana = function () {
      var check = document.getElementById("nickGracza").getElementsByTagName("input")[0].value;
      var znaki = 0;
      for (i = 0; i < check.length; i++) {
        if (
          check[i] == "~" ||
          check[i] == "`" ||
          check[i] == "!" ||
          check[i] == "@" ||
          check[i] == "#" ||
          check[i] == "$" ||
          check[i] == "%" ||
          check[i] == "^" ||
          check[i] == "&" ||
          check[i] == "*" ||
          check[i] == "(" ||
          check[i] == ")" ||
          check[i] == "-" ||
          check[i] == "_" ||
          check[i] == "=" ||
          check[i] == "+" ||
          check[i] == "{" ||
          check[i] == "}" ||
          check[i] == "[" ||
          check[i] == "]" ||
          check[i] == "|" ||
          check[i] == "/" ||
          check[i] == "\n" ||
          check[i] == ":" ||
          check[i] == ";" ||
          check[i] == "'" ||
          check[i] == "<" ||
          check[i] == "." ||
          check[i] == ">" ||
          check[i] == "," ||
          check[i] == "?"
        ) {
          //mozliwosc wpisania tylko liter
          document.getElementById("warning").innerHTML = "Pseudonim nie może zawierać znaków specjalnych!";
          document.getElementById("warning").style.display = "block";
          znaki = 1;
        }
      }
      if (check == "") {
        //gdy ktos nie wpisze nicku
        document.getElementById("warning").innerHTML = "Podaj pseudonim!";
        document.getElementById("warning").style.display = "block";
      } else if (check == "") {
        //mozliwosc wpisania tylko liter
        document.getElementById("warning").innerHTML = "Podaj pseudonim!";
        document.getElementById("warning").style.display = "block";
      } else if (znaki == 0) {
        //wpisanie nicku: ustawienie ciasteczka
        var d = new Date();
        d.setTime(d.getTime() + 70 * (24 * 60 * 60 * 1000)); //dni do zużycia ciasteczka
        var expires = "expires=" + d.toUTCString();

        renderN.nick = check;
        document.getElementById("overlayWygrana").style.display = "none";
        var id1 = Math.floor(Math.random() * 10);
        var id2 = Math.floor(Math.random() * 10);
        var id3 = Math.floor(Math.random() * 10);
        var id4 = Math.floor(Math.random() * 10);
        var id5 = Math.floor(Math.random() * 10);
        var id6 = Math.floor(Math.random() * 10);
        idGracza1 = "" + id1 + id2 + id3 + id4 + id5 + id6;
        document.cookie = idGracza1 + renderN.wymiarr + renderN.nick + "=" + renderN.czasGracza + ";" + expires;
      }
    };
    wyjscieWyniki = function () {
      document.getElementById("overlayWyniki").style.display = "none";
    };
    otwarcieWyniki = function () {
      document.getElementById("overlayWyniki").style.display = "block";
    };
    renderWyniki = function () {
      function leadingZero(i) {
        return i < 10 ? "0" + i : i;
      }
      for (i = 0; i < 4; i++) {
        //reset koloru przyciskow
        document.getElementById("buttonWynik" + i).classList.remove("selected");
      }
      this.classList.add("selected"); //dodanie klasy z kolorem

      renderN.trybGry = this.innerHTML[5]; //okreslenie ze chodzi o dany tryb
      document.getElementById("tabelaWyniki").innerHTML = ""; //czyszczenie tablicy
      renderN.uzupelnienie0 = 0; //zmienna do uzupelnienia tabeli pustymi polami

      var tabela = document.createElement("table");
      tabela.id = "top10tabela";
      document.getElementById("tabelaWyniki").appendChild(tabela);
      // tabela.innerHTML = renderN.trybGry
      var tr1 = document.createElement("tr"); //pierwszy rzad z tytulami kolumn
      tr1.id = "tr0";
      tr1.innerHTML = "<th>lp.</th><th>nick</th><th>czas</th>";
      tabela.appendChild(tr1);

      var tab = document.cookie.split("; "); //rozdzielenie ciastek
      var tabelaDane = []; //tablica do sortowania czasów
      for (i = 0; i < tab.length; i++) {
        pojedynczeCiastko = tab[i].split("=");
        czas = pojedynczeCiastko[1];
        tabelaDane.push(czas); //tablica do sortowania czasów
      }
      uwu = tabelaDane.sort(function (a, b) {
        return a - b;
      }); //posortowanie czasów od najkrotszego
      if (uwu[0] != undefined) {
        //gdy wchodzimy na strone pierwszy raz - nie wykonuje się
        for (i = 0; i < uwu.length; i++) {
          //wykonanie petli tworzenia pozycji max 10 razy (bo top10)
          for (j = 0; j < tab.length; j++) {
            porownanie = tab[j].split("=");
            nick = porownanie[0].slice(7);
            tryb = porownanie[0].slice(6, 7);
            czas = porownanie[1];
            // sek = parseInt(czas)
            var milisekundy = czas.slice(-3);
            var sekundy = Math.floor(parseInt(czas) / 1000);
            var minuty = Math.floor(parseInt(czas) / 1000 / 60);
            var godziny = Math.floor(parseInt(czas) / 1000 / 60 / 60);
            if (sekundy >= 60) {
              sekundy = sekundy - 60 * minuty;
            }
            if (minuty >= 60) {
              minuty = minuty - 60 * godziny;
            }
            czasM = leadingZero(godziny) + ":" + leadingZero(minuty) + ":" + leadingZero(sekundy) + "." + leadingZero(milisekundy);
            if (uwu[i] == porownanie[1]) {
              if (tryb == renderN.trybGry) {
                if (renderN.uzupelnienie0 < 10) {
                  var tr2 = document.createElement("tr");
                  tr2.id = "tr" + (renderN.uzupelnienie0 + 1);
                  tr2.innerHTML =
                    "<td class='rzad1'>" +
                    (renderN.uzupelnienie0 + 1) +
                    "</td><td class='rzad2'>" +
                    nick +
                    "</td><td class='rzad3'>" +
                    czasM +
                    "</td>";
                  tabela.appendChild(tr2);
                  renderN.uzupelnienie0 = renderN.uzupelnienie0 + 1;
                }
              }
            }
          }
        }
      }
      for (i = 0; i < 10 - renderN.uzupelnienie0; i++) {
        var tr3 = document.createElement("tr");
        tr3.id = "tr" + (i + 1 + renderN.uzupelnienie0);
        tr3.innerHTML =
          "<td class='rzad1'>" + (i + 1 + renderN.uzupelnienie0) + "</td><td class='rzad2'>---</td><td class='rzad3'>00:00:00.000</td>";
        tabela.appendChild(tr3);
      }
    };

    for (i = 0; i < 4; i++) {
      //listenery do renderu wylosowanego zdjecia na przyciskach do wyboru trybu (3x3, itd.)
      var button = document.getElementById("button" + i);
      button.addEventListener("click", renderZdjecia);
    }
    for (i = 0; i < 2; i++) {
      //listenery do zmiany zdjecia u gory
      var zdjecieWybrane = document.getElementById("strzalka" + i);
      zdjecieWybrane.addEventListener("click", zmiana);
    }
    for (i = 0; i < 4; i++) {
      //listenery do zmiany trybu w tablicy wynikow
      var button = document.getElementById("buttonWynik" + i);
      button.addEventListener("click", renderWyniki);
    }
    document.getElementById("wyjscieWygrana").addEventListener("click", wyjscieWygrana);
    document.getElementById("wyjscieWyniki").addEventListener("click", wyjscieWyniki);
    document.getElementById("otwarcieWyniki").addEventListener("click", otwarcieWyniki);
  },
  renderStrony: function () {
    renderN.main();
    renderN.reszta();
    renderN.los();
  },
};
renderN.renderStrony();

var clickN = {
  listenery: function () {
    klik = function () {
      //przesuniecie elementu przy czarnym po pierwszym ruchu po losowaniu
      renderN.tabCheck = []; //czyszczenie tablicy do sprawdzania wygranej
      var czarny = document.getElementsByClassName("czarny")[0];
      var topCzarny = czarny.style.top;
      var leftCzarny = czarny.style.left;

      var topSibling = this.style.top;
      var leftSibling = this.style.left;
      czarnyOldID = czarny.id; //zamiana wspolrzednych
      SiblingOldID = this.id;
      czarny.style.top = topSibling; //zamiana pozycji czarnego
      czarny.style.left = leftSibling;
      czarny.id = SiblingOldID;
      this.style.top = topCzarny; //zamiana pozycji sąsiadującego
      this.style.left = leftCzarny;
      this.id = czarnyOldID;
      for (var i = 0; i < 40; i++) {
        //usuniecie listenerow z wszystkich divow na planszy
        var delListener = document.getElementsByClassName("loss")[i];
        if (delListener != undefined) {
          delListener.removeEventListener("click", klik);
        }
      }
      //dodanie listenerow naokolo czarnego
      var SiblingG = document.getElementById("zd" + (parseInt(czarny.id[2]) - 1) + czarny.id[3]);
      if (SiblingG != undefined) {
        SiblingG.addEventListener("click", klik);
      }
      var SiblingP = document.getElementById("zd" + czarny.id[2] + (parseInt(czarny.id[3]) + 1));
      if (SiblingP != undefined) {
        SiblingP.addEventListener("click", klik);
      }
      var SiblingD = document.getElementById("zd" + (parseInt(czarny.id[2]) + 1) + czarny.id[3]);
      if (SiblingD != undefined) {
        SiblingD.addEventListener("click", klik);
      }
      var SiblingL = document.getElementById("zd" + czarny.id[2] + (parseInt(czarny.id[3]) - 1));
      if (SiblingL != undefined) {
        SiblingL.addEventListener("click", klik);
      }
      for (i = 0; i < renderN.wymiarr; i++) {
        //WYGRANA
        for (j = 0; j < renderN.wymiarr; j++) {
          var poprawnosc = document.getElementById("zd" + i + j).classList[1];
          renderN.tabCheck.push(poprawnosc);
          var aaa = 0;
          for (h = 0; h < renderN.tabDefault.length; h++) {
            defaultt = renderN.tabDefault[h];
            checkk = renderN.tabCheck[h];
            if (defaultt == checkk) {
              aaa++;
              if (aaa == renderN.tabDefault.length) {
                renderN.wygrana = 1;
                document.getElementsByClassName("czarny")[0].classList.remove("czarny");
                document.getElementById("overlayWygrana").style.display = "block";
                document.getElementById("warning").style.display = "none";
              }
            }
          }
        }
      }
    };
    listeneryPrzyRenderze = function () {
      //dodanie listenerow po wylosowaniu miejsc
      var stop = setInterval(losRender, 1);
      function losRender() {
        for (var i = 0; i < 40; i++) {
          //usuniecie listenerow
          var delListener = document.getElementsByClassName("loss")[i];
          if (delListener != undefined) {
            delListener.removeEventListener("click", klik);
          }
        }
        //dodanie listenerow naokolo czarnego
        var czarny = document.getElementsByClassName("czarny")[0];
        var SiblingG = document.getElementById("zd" + (parseInt(czarny.id[2]) - 1) + czarny.id[3]);
        if (SiblingG != undefined) {
          SiblingG.removeEventListener("click", klik);
          SiblingG.addEventListener("click", klik);
        }
        var SiblingP = document.getElementById("zd" + czarny.id[2] + (parseInt(czarny.id[3]) + 1));
        if (SiblingP != undefined) {
          SiblingP.removeEventListener("click", klik);
          SiblingP.addEventListener("click", klik);
        }
        var SiblingD = document.getElementById("zd" + (parseInt(czarny.id[2]) + 1) + czarny.id[3]);
        if (SiblingD != undefined) {
          SiblingD.removeEventListener("click", klik);
          SiblingD.addEventListener("click", klik);
        }
        var SiblingL = document.getElementById("zd" + czarny.id[2] + (parseInt(czarny.id[3]) - 1));
        if (SiblingL != undefined) {
          SiblingL.removeEventListener("click", klik);
          SiblingL.addEventListener("click", klik);
        }
        if (renderN.koniecRenderu == 1) {
          clearInterval(stop);
          renderN.koniecRenderu = 0;
        }
      }
    };
    for (i = 0; i < 4; i++) {
      var button = document.getElementById("button" + i);
      button.addEventListener("click", listeneryPrzyRenderze);
    }
  },
};
clickN.listenery();

//addons

//przesuwanie licznika
dragElement(document.getElementById("pojemnikLicznik"));
// dragElement(document.getElementById("motywPojemnik"));

function dragElement(elmnt) {
  var pos1 = 0,
    pos2 = 20,
    pos3 = 0,
    pos4 = 0;
  if (document.getElementById(elmnt.id + "header")) {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "header").onmousedown = dragMouseDown;
  } else {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }
  function dragMouseDown(e) {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3 = e.clientX;
    pos4 = e.clientY;
    document.onmouseup = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }
  function elementDrag(e) {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1 = pos3 - e.clientX;
    pos2 = pos4 - e.clientY;
    pos3 = e.clientX;
    pos4 = e.clientY;
    // set the element's new position:
    elmnt.style.top = elmnt.offsetTop - pos2 + "px";
    elmnt.style.left = elmnt.offsetLeft - pos1 + "px";
  }
  function closeDragElement() {
    // stop moving when mouse button is released:
    document.onmouseup = null;
    document.onmousemove = null;
  }
}

//zmiana kolorów
// var html = document.getElementsByTagName('html')[0];
// function kolorek() {
//     var x = document.getElementById("kolor").value;
//     html.style.setProperty("--kolor-ramek", x);
//     html.style.setProperty("--przezroczystosc-tla", (x + "44"));
//     html.style.setProperty("--hover-przyciskow", (x + "33"));
//     html.style.setProperty("--wybrany-tryb", (x + "88"));
// }

// #motyw1 {
//     background: linear-gradient(45deg, #23D5AB 50%, #f4ecf7 50%)
// }
// #motyw2 {
//     background: linear-gradient(45deg, #500b7e 50%, #9c769c 50%)
// }
// #motyw3 {
//     background: linear-gradient(45deg, #c75824 50%, #f5dda9 50%)
// }

//motywy
var html = document.getElementsByTagName("html")[0];
function motyw() {
  if (this.id == "motyw1") {
    //zielony
    var x = "#f4ecf7"; //glowny
    var y = "#04b98f"; //tlo
  }
  if (this.id == "motyw3") {
    //czerwony
    var x = "#f8a7a7"; //glowny
    var y = "#6e0202"; //tlo
  }
  if (this.id == "motyw2") {
    //pomaranczowy
    var x = "#ffeabe"; //glowny
    var y = "#c75824"; //tlo
  }

  html.style.setProperty("--kolor-ramek", x);
  html.style.setProperty("--przezroczystosc-tla", x + "44");
  html.style.setProperty("--hover-przyciskow", x + "33");
  html.style.setProperty("--wybrany-tryb", x + "88");
  html.style.setProperty("--glowny-kolor", y);
}

for (var i = 0; i < 3; i++) {
  document.getElementsByClassName("motyw")[i].addEventListener("click", motyw);
}
